package com.app.lcc.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import com.app.lcc.Data
import com.app.lcc.R
import com.app.lcc.WebViewClientImpl
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        settings()

    }

    private fun settings(){
        webView.loadUrl(Data.site)

        val webViewClientImpl = WebViewClientImpl(this)
        webView.webViewClient = webViewClientImpl

        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            webView.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}
